'use strict';

const chai = require('chai');
const counter = require('chai-counter');
const assert = chai.assert;
const expect = chai.expect;

const {
    setDom,
    testElementContainCSSClass,
    testDisplayFunction,
    setMockXMLHttpRequest,
    sendRequestHandler
} = require('../utils');

const path = '../../../js/main';


describe('Testing main script - sending request',()=>{

    describe('test sendRequest', () => {
        beforeEach(()=>{
            setDom();
            setMockXMLHttpRequest(200,'Success');
        });

        it('is succeed',(done)=>{
            const {sendRequest} = require(path);

            sendRequestHandler(sendRequest, 'login.php',
                (response)=>{
                    const decodedResponse = JSON.parse(response);
                    expect('Success').to.equal(decodedResponse.message);
                    done();

                },
                ()=>{}
            );
            counter.expect(1);
        });
    });

    describe('test sendRequest', () => {
        beforeEach(()=>{
            setDom();
            setMockXMLHttpRequest(400,'Failure');
        });

        it('is failed',(done)=>{
            const {sendRequest} = require(path);

            sendRequestHandler(sendRequest, 'login.php',
                ()=>{},
                (response)=>{
                    const decodedResponse = JSON.parse(response);
                    expect('Failure').to.equal(decodedResponse.message);
                    done();
                }
            );
            counter.expect(1);
        });
    });
});

describe('Testing main script - dom interaction',()=>{

    beforeEach(()=>{
        setDom();
    });

    it('test formError',()=>{
        const {formError} = require(path);
        const message = "Error!"
        const elem = formError(message);

        expect(elem.innerText).to.equal(message);
        expect(elem.classList.contains('form-error')).to.equal(true);
    });

    it('test cleanFormErrors and showFormErrors with response',()=>{
        const {cleanFormErrors, showFormErrors} = require(path);
        const parentClass = '#sign-in-modal';
        const modelName = 'user';
        const signInModel =document.querySelector(`${parentClass}`);
        const emailElement = signInModel.querySelector(`.input.${modelName}_email`);
        const passwordElement = signInModel.querySelector(`.input.${modelName}_password`);
        const response = {"errors":{"password":["Passwords do not match!"],"email":["Email is already taken!"]}};
        showFormErrors(response, modelName, parentClass);

        expect(emailElement.classList.contains('with-error')).to.equal(true);
        expect(passwordElement.classList.contains('with-error')).to.equal(true);

        const emailError = emailElement.querySelector('p');
        const passwordError = passwordElement.querySelector('p');

        expect(emailError.classList.contains('form-error')).to.equal(true);
        expect(passwordError.classList.contains('form-error')).to.equal(true);

        expect(emailError.innerText).to.equal(...response.errors.email);
        expect(passwordError.innerText).to.equal(...response.errors.password);

        cleanFormErrors('#sign-in-modal');

        const emailError1 = emailElement.querySelector('p');
        const passwordError1 = passwordElement.querySelector('p');

        expect(emailError1).to.be.null;
        expect(passwordError1).to.be.null;

        expect(emailElement.classList.contains('form-error')).to.equal(false);
        expect(passwordElement.classList.contains('form-error')).to.equal(false)

    });

    it('test showFormErrors without response',()=>{
        const {showFormErrors} = require(path);
        showFormErrors(null, 'user', '#sign-in-modal');
        testDisplayFunction({message:'An error occured', type: 'alert'},null,'alert');
    });

    it('test capitalize function', ()=>{
        const {capitalize} = require(path);
        const text= "lorem ipsum";
        assert.equal(capitalize(text),"Lorem ipsum");
    })

    it('test startLoadingAnimation and stopLoadingAnimation functions',()=>{
        const {startLoadingAnimation,stopLoadingAnimation} = require(path);
        testElementContainCSSClass('center-panel','loading-panel', startLoadingAnimation, true);
        testElementContainCSSClass('center-panel','loading-panel', stopLoadingAnimation, false);
    });

    it('test showOverlay and hideOverlay',()=>{
        const {showOverlay,hideOverlay} = require(path);
        testElementContainCSSClass('overlay','hidden', showOverlay, false);
        testElementContainCSSClass('overlay','hidden', hideOverlay, true);
    });

    it('test showLoginModal and hideLoginModal',()=>{
        const {showLoginModal,hideLoginModal} = require(path);
        testElementContainCSSClass('login-modal','hidden', showLoginModal, false);
        testElementContainCSSClass('login-modal','hidden', hideLoginModal, true);
    });

    it('test showSignInModal and hideSignInModal',()=>{
        const {showSignInModal,hideSignInModal} = require(path);
        testElementContainCSSClass('sign-in-modal','hidden', showSignInModal, false);
        testElementContainCSSClass('sign-in-modal','hidden', hideSignInModal, true);
    });

    it('test toggleUserMenu',()=>{
        const {toggleUserMenu} = require(path);
        const userMenu = document.getElementById('user-menu');
        const flag = userMenu.classList.contains('hidden');

        testElementContainCSSClass('user-menu','hidden',toggleUserMenu, !flag);
        testElementContainCSSClass('user-menu','hidden',toggleUserMenu, !!flag);

    });

    it('test displayInfo',()=>{
        const {displayInfo} = require(path);
        testDisplayFunction({message:"Info text"},displayInfo);
    });

    it('test displayNotice',()=>{
        const {displayInfo} = require(path);
        testDisplayFunction({message:"Notice text", type:'notice'},displayInfo);
    });

    it('test displayAlert',()=>{
        const {displayInfo} = require(path);
        testDisplayFunction({message:"Alert text", type: 'alert'},displayInfo,'alert');
    });

    it('test setCanvasSize',()=>{
        const {setCanvasSize} = require(path);
        const width= 100;
        const height = 150;
        const canvas = document.getElementById('canvas');

        setCanvasSize(width,height)

        expect(canvas.style.width).to.equal(`${width}px`);
        expect(canvas.width).to.equal(width);
        expect(canvas.height).to.equal(height);
    });

    it('test drawScaledPredictionRectangle',()=>{
        const {drawScaledPredictionRectangle} = require(path);
        const div = document.querySelector('#center-panel div.card-image')
        div.appendChild(document.createElement('img'))

        drawScaledPredictionRectangle({bbox:[14, 32, 200, 136]});

        const canvas = document.querySelector('#canvas')
        const ctx = canvas.getContext("2d")

        expect(ctx.shadowBlur).to.equal(5);
        expect(ctx.lineWidth).to.equal(3);
        expect(ctx.shadowColor).to.equal('#000000');
    });

});