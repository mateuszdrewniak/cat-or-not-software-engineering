const jsdom = require("jsdom");
const { JSDOM } = jsdom;

// Fake DOM
module.exports = new JSDOM(`
<html>
    <head></head>
<body class="bg-light-gray text-black">
  <div id="info" class="info hidden">
    <p></p>
  </div>

  <!-- ########### MODALS ########### -->
  <div id="overlay" class="hidden"></div>

  <div id="left-catalog-modal" class="catalog-modal modal card shadow bg-white hidden">
    <h3 class="modal-header">Not Cats</h3>
    <div class="modal-body">
      <div class="empty-message">
        No images yet!
      </div>
    </div>
  </div>

  <div id="right-catalog-modal" class="catalog-modal modal card shadow bg-white hidden">
    <h3 class="modal-header">Cats</h3>
    <div class="modal-body">
      <div class="empty-message">
        No images yet!
      </div>
    </div>
  </div>

    <!-- Sign In Modal -->
    <div id="sign-in-modal" class="modal card shadow bg-white hidden">
      <h3 class="modal-header">Sign In</h3>
      <div class="modal-body">
        <form action="./sign_in.php" method="post">
          <div class="input user_email">
            <label for="sign-in-email">Email</label>
            <input type="email" name="email" id="sign-in-email" required="true">
          </div>
          <div class="input user_password">
            <label for="sign-in-password">Password</label>
            <input type="password" name="password" id="sign-in-password" required="true">
          </div>
          <div class="input">
            <label for="sign-in-password-confirmation">Password Confirmation</label>
            <input type="password" name="password-confirmation" id="sign-in-password-confirmation" required="true">
          </div>
    
          <div class="input">
            <input type="submit" name="commit" value="Sign In" class="btn bg-blue text-white shadow rounded-full">
          </div>
        </form>
        <div id="login-link" class="link">Log in</div>
      </div>
    </div>

    <!-- Log In Modal -->
    <div id="login-modal" class="modal card shadow bg-white hidden">
      <h3 class="modal-header">Log In</h3>
      <div class="modal-body">
        <form action="./login.php" method="post">
          <div class="input">
            <label for="login-email">Email</label>
            <input type="email" name="email" id="login-email" required="true">
          </div>
          <div class="input">
            <label for="login-password">Password</label>
            <input type="password" name="password" id="login-password" required="true">
          </div>
    
          <div class="input">
            <input type="submit" name="commit" value="Log In" class="btn bg-blue text-white shadow rounded-full">
          </div>
        </form>
        <div id="sign-in-link" class="link">Sign in</div>
      </div>
    </div>

    <!-- User Menu -->
    <div id="user-menu" class="card shadow bg-white hidden">
      <div class="input">
        <label>User</label>
        <div class="input-mock disabled"></div>
      </div>
      <a href="./logout.php" id="logout-btn" class="btn shadow center bg-blue text-white rounded-full mb-2">Log Out</a>
      <div id="destroy-session-btn" class="btn shadow center bg-red text-white rounded-full">Destroy Session</div>
    </div>
  <!-- ########### MAIN PAGE ########### -->

  <header class="flex">
    <div class="flex-1">
        <img id="avatar" class="rounded-full" src="https://www.gravatar.com/avatar/">
        <div id="login-btn" class="btn shadow center bg-white rounded-full">Log In</div>
    </div>
    <div id="logo" class="center-panel flex-1">
      Cat or Not?
    </div>
    <div class="flex-1">

    </div>
  </header>

  <main class="flex">
    <section id="left-panel" class="panel flex-1">
      <div class="image-card-wrapper">
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div id="left-catalog-cover" class="image-card"></div>
        <div class="image-card-wrapper-title">Not cats</div>
      </div>
    </section>

    <section id="center-panel" class="center-panel loading-panel panel flex-1 relative">
      <div class="card shadow bg-white mb-2">
        <div class="card-image relative">
          <canvas id="canvas"></canvas>
          <div class="placeholder-image"></div>
          <div class="loading-spinner spinner">
            <i class="fas fa-circle-notch"></i>
          </div>
        </div>

        <div class="predictions-wrapper">
          <h3>Predictions:</h3>
          <div id="predictions">Cat</div>
        </div>

        <div class="loading-predictions-wrapper">
          <div class="loading-text"></div>
          <div class="loading-text"></div>
        </div>
      </div>
      <div id="upload-btn" class="upload-btn btn shadow mx-auto center bg-blue text-white rounded-full">Choose a Photo</div>
    </section>

    <section id="right-panel" class="panel flex-1">
      <div class="image-card-wrapper">
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div id="right-catalog-cover" class="image-card"></div>
        <div class="image-card-wrapper-title">Cats</div>
      </div>
    </section>
  </main>

  <form class="hidden" enctype="multipart/form-data">
    <input type="file" name="image_input" id="image-input" accept="image/*">
  </form>

  <script src="js/main.js"></script>
</body>
</html>

`);