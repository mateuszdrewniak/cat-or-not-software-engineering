const MockXMLHttpRequest = require('mock-xmlhttprequest');
const assert = require('chai').assert;
const expect = require('chai').expect;

const setDom = () =>{
    const dom = require('./dom');
    global.window = dom.window;
    global.document = dom.window.document;
};

const testElementContainCSSClass = (idElement,className,cb,result)=>{
    const elem = document.getElementById(idElement);
    cb();
    const res = elem.classList.contains(className);
    expect(res).to.equal(result);
}

const testDisplayFunction = (msg,cb,className='notice')=>{
    const info = document.getElementById('info');
    if(cb){
        cb(msg);
    }

    const responseText = info.querySelector('p').innerHTML;
    assert.equal(msg.message, responseText);

    if(msg.type){
        // console.log("in type" +className);
        expect(info.classList.contains(className)).to.equal(true);
    }
}

const setMockXMLHttpRequest = (status,msg) =>{
    const MockXhr = MockXMLHttpRequest.newMockXhr();

    MockXhr.onSend = (xhr) => {
        const responseHeaders = { 'Content-Type': 'application/json' };
        const response = `{ "message": "${msg}" }`;
        xhr.respond(status, responseHeaders, response);
    };

    global.XMLHttpRequest = MockXhr;
}

const sendRequestHandler = (cb,path,positive=()=>{},negative=()=>{}) =>{
    const payload = {
        email: "test@test.pl",
        password: "test1234"
    };

    cb({
        path: path,
        payload: JSON.stringify(payload),
        positive: positive,
        negative: negative
    });
}

module.exports ={
    setDom,
    setMockXMLHttpRequest,
    testElementContainCSSClass,
    testDisplayFunction,
    sendRequestHandler,
}