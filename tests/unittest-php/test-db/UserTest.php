<?php
declare(strict_types=1);

require_once('./php/user.php');
require_once('./tests/unittest-php/BaseUserDBTest.php');

class UserTest extends BaseUserDBTest{

    public function testFindByCredentialsUser(){
        $fakeUser = new FakeUser();
        $fakeUser->email=$this->data['email'];
        $fakeUser->password=$this->data['password'];
        $user = User::find_by_credentials($fakeUser);
        $this->assertNotNull($user);
        $this->assertEquals($user->email,$this->data['email']);
    }

    public function testFindUser(){
        $user = $this->user->find($this->user->id);
        $this->assertNotNull($user);
        $this->assertEquals($user->email,$this->data['email']);
    }

    public function testWhere(){
        $email = $this->data['email'];
        User::$table_name='users';
        $user = User::where("email LIKE '".$email."'");
        $this->assertNotNull($user);
        $this->assertEquals(count($user),1);
        $this->assertEquals($user[0]->email,$email);
    }
}

class FakeUser{
    public $email;
    public $password;
}