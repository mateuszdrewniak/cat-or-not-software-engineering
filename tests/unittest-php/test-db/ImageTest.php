<?php
declare(strict_types=1);

require_once('./php/image.php');
require_once('./tests/unittest-php/BaseUserDBTest.php');

class ImageTest extends BaseUserDBTest{
    private $image;
    private $imageData;

    public function setUp():void{
        parent::setUp();

        $this->imageData=[
            'created_at'=> new DateTime('2014-09-12 19:23:01'),
            'updated_at'=> new DateTime('2014-09-20 11:13:34'),
            'best_prediction'=>'cat',
            'name'=>"cat",
            'extension'=>'jpg',
            'user_id'=>$this->user->id,
            'id'=>1
        ];

        $this->image = new Image($this->imageData);
        $this->image->persist();
    }

    public function tearDown():void{
        $this->image->delete();
        parent::tearDown();
    }

    public function testSetName(){
        $nameBeforeChange = $this->image->name;
        $this->image->set_name('jpeg');
        $nameAfterChange=$this->image->name;

        $this->assertNotNull($nameAfterChange);
        $this->assertNotEquals($nameBeforeChange,$nameAfterChange);
    }

    public function testFindById(){
        $image = $this->image->find($this->image->id);
        $this->assertNotNull($image);
    }

    public function testWhere(){
        $email = $this->data['email'];
        User::$table_name='users';
        $user = User::where("email LIKE '".$email."'");
        $this->assertNotNull($user);
        $this->assertEquals(count($user),1);
        $this->assertEquals($user[0]->email,$email);
    }
}
