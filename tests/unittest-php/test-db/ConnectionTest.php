<?php
declare(strict_types=1);

require_once('php/connection.php');

use PHPUnit\Framework\TestCase;

class ConnectionTest extends TestCase{
    public function testConnection(){
        $connection = new Connection();
        $this->assertNotNull($connection);
    }
}