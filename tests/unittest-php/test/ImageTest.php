<?php
declare(strict_types=1);

require_once('php/image.php');
require_once('tests/unittest-php/BaseTest.php');

class ImageTest extends BaseTest{
    private $image;
    private $data;

    public function setUp():void{
        $this->data=[
            'best_prediction'=>'cat',
            'name'=>"cat",
            'extension'=>'jpg',
            'user_id'=>1,
            'id'=>1
        ];

        $this->image = new Image($this->data,false);
    }

    public function testCat() {
        $this->assertTrue($this->image->cat());
    }

    public function testLocation() {
        $this->assertEquals($this->image->location(),"uploads/".$this->image->name);
    }

    public function testAddress() {
        $this->assertEquals($this->image->address(),"image.php?name=".$this->image->name);
    }

    public function testToJson() {
        $jsonArray = $this->image->to_json();
        $keys=[''];
        $this->assertEquals($jsonArray['path'],$this->image->location());
        $this->assertEquals($jsonArray['user_id'],$this->data['id']);
    }

    public function testValid() {
        $image = new Image([],false);

        $result = $image->valid();
        $errors = $this->getValueOfPrivateFieldOfClass(Image::class,'errors',$image);

        $expectedErrors=[
            'user'=>'User is incorrect or does not exist!',
            'name'=>'File name is incorrect!',
            'best_prediction'=>'Best prediction must not be empty!'
        ];

        // var_dump($errors);
        foreach($expectedErrors as $key => $value){
            $this->assertEquals($errors[$key][0],$value);
        }
        $this->assertFalse($result);

    }
}
