<?php
declare(strict_types=1);

require_once('./php/active_record.php');
require_once('./tests/unittest-php/BaseTest.php');

class ActiveRecordTest extends BaseTest{

    public function testGeneratorGuid(){
        $value = ActiveRecord::guid();
        $this->assertEquals(preg_match('/([a-f0-9-])/',$value),1);
    }

    public function testAllAttributeNames(){
        $result = ActiveRecord::all_attribute_names();
        $this->assertEquals(count($result),3);
        $reflection = new ReflectionClass(ActiveRecord::class);
        
        foreach($result as $value){
            $property = $reflection->getProperty($value);
            $this->assertEquals($property->getName(),$value);
        }
    }

    public function testAttributeNameList(){
        ActiveRecord::$attribute_names = ActiveRecord::all_attribute_names();
        $result = ActiveRecord::attribute_name_list();
        $this->assertEquals($result,"id, created_at, updated_at");
        ActiveRecord::$attribute_names = [];

    }

    public function testAttributeQuoteLsist(){
        ActiveRecord::$attribute_names = ActiveRecord::all_attribute_names();
        $result = ActiveRecord::attribute_quote_list();
        $this->assertEquals($result,"?, ?, ?");
        ActiveRecord::$attribute_names = [];
    }

    public function testAddError(){
        $activeRecord = new ActiveRecord([]);
        $errors = $this->getValueOfPrivateFieldOfClass(ActiveRecord::class,'errors',$activeRecord);
        $this->assertTrue(empty($errors));

        $this->addError($activeRecord,'id','Incorrect id!');

        $errors = $this->getValueOfPrivateFieldOfClass(ActiveRecord::class,'errors',$activeRecord);
        $this->assertFalse(empty($errors));
    }

    public function testValidFunction(){
        $activeRecord = new ActiveRecord([]);
        $this->assertTrue($activeRecord->valid());

        $this->addError($activeRecord,'id','Incorrect id!');
        $this->assertFalse($activeRecord->valid());
    }

    //helper functions
    
    public function addError($activeRecord, $param, $msg){
        $reflectionMethod = new ReflectionMethod(ActiveRecord::class, 'add_error');
        $reflectionMethod->setAccessible(true);
        $reflectionMethod->invoke($activeRecord,$param,$msg);
    }

}