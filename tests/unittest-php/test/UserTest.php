<?php
declare(strict_types=1);

require_once('./php/user.php');
require_once('./tests/unittest-php/BaseTest.php');


class UserTest extends BaseTest{

    public $user;
    public $data;

    public function setUp():void{
        $this->data=[
            'id' => 1,
            'email'=>'test@gmail.com',
            'password'=>'test1234',
            'password_confirmation'=>'test1234',
            'created_at'=> new DateTime('2014-09-12 19:23:01'),
            'updated_at'=> new DateTime('2014-09-20 11:13:34')
        ];

        $this->user = new User($this->data);
    }

    // tests

    public function testValidEmail(){
        $validEmail='example@gmail.com';
        $this->user->set_email($validEmail);
        $this->assertEquals($this->user->email,$validEmail);
    }

    public function testInvalidEmail(){
        $invalidEmail='invalid email';
        $this->user->set_email($invalidEmail);
        $this->assertNotEquals($this->user->email,$invalidEmail);
        $this->checkError('email','Email is invalid!');
    }

    public function testValidPassword(){
        $this->setValueOfPrivateFieldOfClass('user','encrypted_password', $this->user,null);
        $this->user->set_password('test1234','test1234');
        $this->assertNotNull(
            $this->getValueOfPrivateFieldOfClass('user','encrypted_password', $this->user)
        );
    }

    public function testInvalidLengthOfPassword(){
        $this->checkPassword('Password is too short (min 6 characters)','test1','test1');
    }

    public function testInvalidPasswordWithoutNumber(){
        $this->checkPassword('Password must include at least one number!','testtest','testtest');
    }

    public function testInvalidPasswordWithoutLetter(){
        $this->checkPassword('Password must include at least one letter!','1234567','1234567');
    }

    
    public function testInvalidPasswordConfirmation(){
        $this->checkPassword('Passwords do not match!','test1234','test4321');
    }

    public function testToJson(){
        $jsonArray = $this->user->to_json();
        // var_dump($jsonArray);

        $keys = ['id','email','created_at','updated_at'];
        foreach($keys as $key){
            $this->assertTrue(array_key_exists($key,$jsonArray));
            $this->assertEquals($jsonArray[$key],$this->data[$key]);
        }
    }
    
    //helper functions

    public function checkPassword($result, $pass1, $pass2){
        $this->setValueOfPrivateFieldOfClass('user','encrypted_password', $this->user,null);
        $this->user->set_password($pass1,$pass2);    
        $this->assertNull(
            $this->getValueOfPrivateFieldOfClass('user','encrypted_password', $this->user)
        );
        $this->checkError('password',$result);
    }

    public function checkError($key, $result){
        $errors = $this->user->get_errors();
        // var_dump($errors);
        if(!empty($errors) && array_key_exists($key,$errors)){
            if(count($errors[$key])==1){
                $this->assertEquals($errors[$key][0],$result);
            }
        }
    }

}
