<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

abstract class BaseUserDBTest extends TestCase{
    protected $user;
    protected $data;

    public function setUp():void{
        $this->data=[
            'id'=>1,
            'email'=>'test@gmail.com',
            'password'=>'test1234',
            'password_confirmation'=>'test1234',
            'created_at'=> new DateTime('2014-09-12 19:23:01'),
            'updated_at'=> new DateTime('2014-09-20 11:13:34')
        ];

        $this->user = new User($this->data);
        $this->user->persist();
    }

    public function tearDown():void{
        $this->user->delete();
    }

}
