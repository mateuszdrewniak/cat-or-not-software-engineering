<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase{

    public function getValueOfPrivateFieldOfClass($className, $fieldName, $obj){
        $reflectionClass  = new ReflectionClass($className);
        $reflectionProperty = $reflectionClass->getProperty($fieldName);
        $reflectionProperty->setAccessible(true);
        return $reflectionProperty->getValue($obj);
    }

    public function setValueOfPrivateFieldOfClass($className, $fieldName, $obj, $value){
        $reflectionClass  = new ReflectionClass($className);
        $reflectionProperty = $reflectionClass->getProperty($fieldName);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($obj, $value);
    }
}
