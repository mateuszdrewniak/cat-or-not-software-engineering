#!/usr/bin/python3

__all__ = (
    'URL',
    'CHROME_DRIVER_PATH',
    'TIMEOUT',
)

###########################  CONFIG  ################################ 
URL = "https://cat-or-not.mateuszdrewniak.it/"
# URL = "http://127.0.0.1:8000/"

# DRIVER CONFIG
CHROME_DRIVER_PATH = "D:\chromedriver\chromedriver.exe"
TIMEOUT = 3