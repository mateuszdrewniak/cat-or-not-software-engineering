#!/usr/bin/python3
import unittest
import time
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


from config import *

class WebTest(unittest.TestCase):
    def getChromeOptions(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--ignore-ssl-errors')
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        return options

    def setUp(self):
        # print("Opening browser...")
        try:
            self.driver = webdriver.Chrome(options=self.getChromeOptions(),executable_path=CHROME_DRIVER_PATH)
            self.driver.get(HOSTNAME)
        except Exception:
            raise Exception('Cannot create web driver!')

    def test_title_and_logo(self):
        # check title
        title = self.driver.title
        self.assertEqual('Cat or Not?'.upper(),title.upper())
        # check logo
        logo = WebDriverWait(self.driver, TIMEOUT).until(EC.presence_of_element_located((By.ID,'logo')))
        # print(logo.text)
        self.assertEqual('Cat or Not?'.upper(),logo.text.upper())
        time.sleep(1)

    def test_upload_file_cat_img(self):
        self.upload_file('../../images/example-cat.jpg','cat')
    
    def test_upload_file_dog_img(self):
        self.upload_file('../../images/example-dog.jpg','dog')

    def upload_file(self,pathname,prediction_result):
        upload_file = WebDriverWait(self.driver,TIMEOUT).until(EC.presence_of_element_located((By.ID,'image-input')))
        path = os.path.abspath(pathname)

        upload_file.send_keys(path)
        section = self.driver.find_element_by_id('center-panel')
        WebDriverWait(section,TIMEOUT*40).until(EC.invisibility_of_element_located((By.CSS_SELECTOR,'div.loading-spinner')))
        prediction= self.driver.find_element_by_css_selector('div#predictions span')
#         print(prediction.text)
        self.assertEqual(prediction_result.upper(), prediction.text.upper())
        time.sleep(5)

    def test_sign_in_and_login(self):
        password = "test1234"
        email=f'test-{int(time.time())}@test.pl'

        self.sign_in(email,password)
        self.logout()
        self.login(email,password)

    def sign_in(self,email,password):
        self.click_login_btn()
        WebDriverWait(self.driver,TIMEOUT).until(EC.visibility_of_element_located((By.ID,'login-modal')))
        signupBtn = self.driver.find_element_by_id('sign-in-link')
        signupBtn.click()
        
        WebDriverWait(self.driver,TIMEOUT).until(EC.presence_of_element_located((By.ID,'sign-in-email')))
        emailInput =  self.driver.find_element_by_id('sign-in-email')
        passInput = self.driver.find_element_by_id('sign-in-password')
        passConfInput = self.driver.find_element_by_id('sign-in-password-confirmation')

        emailInput.send_keys(email)
        passInput.send_keys(password)
        passConfInput.send_keys(password)
        time.sleep(1)

        submitBtn = self.driver.find_element_by_css_selector('input[type="submit"]')
        submitBtn.click()
        self.check_message("Account created")
    
    def logout(self):
        avatarLogout = WebDriverWait(self.driver,TIMEOUT).until(EC.presence_of_element_located((By.ID,'avatar')))
        avatarLogout.click()
        
        WebDriverWait(self.driver,TIMEOUT).until(EC.presence_of_element_located((By.ID,'avatar')))
        logoutAnchor = self.driver.find_element_by_css_selector('div#user-menu a')
        self.assertEqual("log out".upper(),logoutAnchor.text.upper())
        logoutAnchor.click()


    def login(self,email,password):
        self.click_login_btn()
        WebDriverWait(self.driver,TIMEOUT).until(EC.visibility_of_element_located((By.CSS_SELECTOR,'div#login-modal')))
        time.sleep(1)
        emailInput =  self.driver.find_element_by_id('login-email')
        passInput = self.driver.find_element_by_id('login-password')

        emailInput.send_keys(email)
        passInput.send_keys(password)
        time.sleep(1)

        submitBtn = self.driver.find_element_by_css_selector('#login-modal input[type="submit"]')
        submitBtn.click()

        self.check_message('logged in')

    def check_message(self,msg):
        time.sleep(2)
        WebDriverWait(self.driver,TIMEOUT).until(EC.presence_of_element_located((By.ID,'info')))
        info = self.driver.find_element_by_css_selector('div#info p')
        self.assertEqual(msg.upper(), info.text.upper())

    def click_login_btn(self):
        WebDriverWait(self.driver,TIMEOUT).until(EC.visibility_of_element_located((By.ID,'login-btn')))
        self.driver.find_element_by_id('login-btn').click()

    def tearDown(self):
        # print("Closing browser...")
        if self.driver is not None:
            self.driver.close()
        

if __name__ == "__main__":
    unittest.main()
