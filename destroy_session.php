<?php
require_once('./php/image.php');
session_start();

if(isset($_SESSION['user_id'])) {
    $images = Image::where('user_id = ?', [$_SESSION['user_id']]);
    foreach($images as $image) {
        unlink($image->location());
    }
    Image::delete_where('user_id = ?', [$_SESSION['user_id']]);
}

header('Location: index.php');

?>
