// ========== Constant definitions ==========
const info = document.querySelector('#info')
const overlay = document.querySelector('#overlay')
const loginModal = document.querySelector('#login-modal')
const logoutBtn = document.querySelector('#logout-btn')
const loginBtn = document.querySelector('#login-btn')
const loginLink = document.querySelector('#login-link')
const signInLink = document.querySelector('#sign-in-link')
const signInModal = document.querySelector('#sign-in-modal')
const destroySessionBtn = document.querySelector('#destroy-session-btn')
const avatar = document.querySelector('#avatar')
const userMenu = document.querySelector('#user-menu')
const uploadBtn = document.querySelector('#upload-btn')
const imageForm = document.querySelector('#image-form')
const imageInput = document.querySelector('#image-input')
const centerPanel = document.querySelector('#center-panel')
const leftPanel = document.querySelector('#left-panel')
const rightPanel = document.querySelector('#right-panel')
const predictionsDiv = document.querySelector('#predictions')
const canvas = document.querySelector('#canvas')
const ctx = canvas.getContext("2d")
const leftCatalogCover = document.querySelector('#left-catalog-cover')
const rightCatalogCover = document.querySelector('#right-catalog-cover')
const leftCatalogModal = document.querySelector('#left-catalog-modal')
const rightCatalogModal = document.querySelector('#right-catalog-modal')
var coco = null

// ========== Function definitions ==========

function sendRequest({ path, payload, positive = ()=>{}, negative = ()=>{}, method = 'POST'} = {}) {
  let xhr = new XMLHttpRequest()
  xhr.open(method, path, true)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if(xhr.status === 200 || xhr.status === 201) {
        // on ready state change
        positive(xhr.response)
      } else {
        negative(xhr.response)
      }
    }
  }
  xhr.send(payload)
}

function loggedIn() {
  return loginBtn == null
}

function attemptLogIn() {
  let payload = {
    email: document.querySelector('#login-email').value,
    password: document.querySelector('#login-password').value
  }

  let positive = (response) => {
    let newUrl =  window.location.href + '?logged_in=true'
    window.location.href = newUrl
  }

  let negative = (response) => {
    response = JSON.parse(response)
    displayAlert(response.error)
  }
  sendRequest({ path: 'login.php', payload: JSON.stringify(payload), positive: positive, negative: negative })
}

function attemptSignIn() {
  cleanFormErrors('#sign-in-modal')
  let payload = {
    email: document.querySelector('#sign-in-email').value,
    password: document.querySelector('#sign-in-password').value,
    password_confirmation: document.querySelector('#sign-in-password-confirmation').value
  }

  let positive = (response) => {
    let newUrl =  window.location.href + '?registered=true'
    window.location.href = newUrl
  }

  let negative = (response) => {
    response = JSON.parse(response)
    displayAlert('Form error')
    showFormErrors(response, 'user', '#sign-in-modal')
  }
  sendRequest({ path: 'sign_in.php', payload: JSON.stringify(payload), positive: positive, negative: negative })
}

function uploadImage() {
  let payload = new FormData(imageForm)
  let mainImage = document.querySelector('#center-panel img')
  payload.append('best_prediction', capitalize(mainImage.dataset.bestPrediction))

  let positive = (response) => {
    console.log(response)
    response = JSON.parse(response)
    console.log(response)
    addImageToCatalog(response.image.path)
  }

  let negative = (response) => {
    console.log(response)
    response = JSON.parse(response)
    console.log(response)
    displayAlert("Can't save image!", 2000)
  }
  sendRequest({ path: 'upload.php', payload: payload, positive: positive, negative: negative })
}

function formError(message) {
  let element = document.createElement("p");
  element.innerText = message
  element.classList.add('form-error')

  return element
}

function cleanFormErrors(parentClass = '') {
  if(parentClass != '')
    parentClass += ' '
  for(let el of document.querySelectorAll(`${parentClass}.form-error`)) {
    el.remove()
  }
  for(let el of document.querySelectorAll(`${parentClass}.input`)) {
    el.classList.remove('with-error')
  }
}

function showFormErrors(data, modelName, parentClass = '') {
  let input, message
  if(parentClass != '')
    parentClass += ' '

  if(data != null && data != '' && data['errors'] != null) {
    for(let key in data['errors']) {
      input = document.querySelector(`${parentClass}.input.${modelName}_${key}`)
      if(input == null) continue
      input.classList.add('with-error')

      data['errors'][key].forEach((error) => {
        message = formError(error)
        input.append(message)
      })
    }
  } else {
    displayAlert('An error occured')
  }
}

function capitalize(string) {
  return string[0].toUpperCase() + string.slice(1)
}

function startLoadingAnimation() {
  centerPanel.classList.add('loading-panel')
}

function stopLoadingAnimation() {
  centerPanel.classList.remove('loading-panel')
}

function showOverlay() {
  document.querySelector('body').classList.add('modal-open')
  overlay.classList.remove('hidden')
}

function hideOverlay() {
  document.querySelector('body').classList.remove('modal-open')
  overlay.classList.add('hidden')
}

function showLoginModal() {
  showOverlay()
  loginModal.classList.remove('hidden')
}

function hideLoginModal() {
  hideOverlay()
  loginModal.classList.add('hidden')
}

function showSignInModal() {
  showOverlay()
  signInModal.classList.remove('hidden')
}

function hideSignInModal() {
  hideOverlay()
  signInModal.classList.add('hidden')
}

function showLeftCatalogModal() {
  showOverlay()
  leftCatalogModal.classList.remove('hidden')
}

function hideLeftCatalogModal() {
  hideOverlay()
  leftCatalogModal.classList.add('hidden')
}

function showRightCatalogModal() {
  showOverlay()
  rightCatalogModal.classList.remove('hidden')
}

function hideRightCatalogModal() {
  hideOverlay()
  rightCatalogModal.classList.add('hidden')
}

function toggleUserMenu() {
  userMenu.classList.toggle('hidden')
}

function hideUserMenu() {
  userMenu.classList.add('hidden')
}

function displayInfo({ message, type = 'notice', displayTime = 500 }) {
  info.classList.add(type)
  info.firstElementChild.innerHTML = message

  info.classList.remove('hidden')
  
  info.style.top = '100px'
  setTimeout(() => {
    info.style.top = '0'
    setTimeout(() => {
      info.classList.add('hidden')
      info.classList.remove(type)
    }, 500)
  }, displayTime)
}

function displayNotice(message, displayTime = 500) {
  displayInfo({ message: message, displayTime: displayTime })
}

function displayAlert(message, displayTime = 500) {
  displayInfo({ message: message, type: 'alert', displayTime: displayTime })
}

// Get the image from the input as Base64
const readURL = file => {
  return new Promise((res, rej) => {
      const reader = new FileReader()
      reader.onload = e => res(e.target.result)
      reader.onerror = e => rej(e)
      if(file != null) reader.readAsDataURL(file)
  })
}

// Set the canvas's size to match that of the image
function setCanvasSize(width, height) {
  canvas.style.width = width.toString() + 'px'
  canvas.width = width
  canvas.height = height
}

// Draw the rectangle encompassing the detected part of the image
function drawScaledPredictionRectangle(prediction, setWidth = false) {
  let mainImage = document.querySelector('#center-panel .card-image img')
  if(mainImage == null) return

  if(setWidth) setCanvasSize(mainImage.offsetWidth, mainImage.offsetHeight)

  let scaledRectangle = prediction.bbox
  ctx.strokeStyle = prediction.class == 'cat' ? '#07bd56' : 'red'
  ctx.lineWidth = 3
  ctx.shadowColor = '#000'
  ctx.shadowBlur = 5
  ctx.strokeRect(scaledRectangle[0], scaledRectangle[1], scaledRectangle[2], scaledRectangle[3])

  let fontSize = 12
  ctx.font = `${fontSize}px Arial`
  ctx.fillStyle = ctx.strokeStyle
  ctx.textAlign = "center"
  ctx.shadowBlur = 0
  let subtitleWidth = ctx.measureText(prediction.class.length).width + 2
  let offsetX = scaledRectangle[0] + fontSize + subtitleWidth
  let offsetY = scaledRectangle[1] + fontSize
  let subtitle = subtitleWidth > scaledRectangle[2] ? prediction.class[0] + '.' : prediction.class
  ctx.fillText(capitalize(subtitle), offsetX, offsetY);
}

function loadImageForAnalysis(url, addToCatalog = true) {
  let cardImage = document.querySelector('#center-panel .card-image')
  let img = document.createElement('img')

  if(!addToCatalog) {
    hideRightCatalogModal()
    hideLeftCatalogModal()
  }
  startLoadingAnimation()

  // Remove old image/images
  let currentImages = document.querySelectorAll('#center-panel .card-image .placeholder-image, #center-panel .card-image img')
  for(let currentImage of currentImages) {
    currentImage.remove()
  }
  // set the source of the new image as the Base64 read from the input
  img.setAttribute('src', url)
  cardImage.appendChild(img)
  setCanvasSize(canvas.width, canvas.height)

  coco = cocoSsd.load();
  coco.then(model => setTimeout(() => {
    model.detect(img).then(predictions => {
      // when no objects where identified
      if(predictions.length == 0) {
        setCanvasSize(img.offsetWidth, img.offsetHeight)
        stopLoadingAnimation()
        let mainImage = document.querySelector('#center-panel img')
        mainImage.dataset.bestPrediction = 'none'
        predictionsDiv.innerHTML = '<span class="text-red">None</span>'

        if(addToCatalog) {
          if(loggedIn()) uploadImage()
          else addImageToCatalog()
        }
        return
      }
      let predictionString = ''
      let first = true
      console.log(predictions)
      // iterate over the predictions
      for(let prediction of predictions) {
        let color = 'text-red'

        let mainImage = document.querySelector('#center-panel img')
        if(first) mainImage.dataset.bestPrediction = prediction.class

        if(prediction.class == 'cat') {
          mainImage.dataset.bestPrediction = 'cat'
          color = 'text-green'
        }

        predictionString += `<span class="${color}">${capitalize(prediction.class)}</span>, `
        drawScaledPredictionRectangle(prediction, first)
        first = false
      }
      // remove the last ', ' from the prediction string
      predictionString = predictionString.slice(0, -2)

      predictionsDiv.innerHTML = predictionString
      stopLoadingAnimation()

      if(addToCatalog) {
        if(loggedIn()) uploadImage()
        else addImageToCatalog()
      }
    })
  }, 100))
}

function addImageToCatalog(src = null) {
  let mainImage = document.querySelector('#center-panel img')

  let imageContainer = document.createElement('div')
  let bestPrediction = document.createElement('div')

  imageContainer.classList.add('image-container')
  bestPrediction.classList.add('best-prediction')
  bestPrediction.append(document.createTextNode(capitalize(mainImage.dataset.bestPrediction)))
  imageContainer.append(bestPrediction)
  let catalogImage = mainImage.cloneNode(true)

  if(src != null) catalogImage.src = src

  imageContainer.append(catalogImage)

  let panel = mainImage.dataset.bestPrediction == 'cat' ? 'right' : 'left'
  let targetCard = document.querySelector(`#${panel}-catalog-cover`)
  let emptyMessage = document.querySelector(`#${panel}-catalog-modal .empty-message`)
  if(emptyMessage != null) emptyMessage.remove()

  let targetRow = document.querySelector(`#${panel}-catalog-modal .catalog-row:first-child`)

  if(targetRow == null || targetRow.childElementCount >= 3) {
    targetRow = document.createElement('div')
    targetRow.classList.add('catalog-row')
    let modalBody = document.querySelector(`#${panel}-catalog-modal .modal-body`)
    modalBody.prepend(targetRow)
  }

  let catalogImageCard = document.createElement('div')
  catalogImageCard.classList.add('image-card')
  catalogImageCard.prepend(imageContainer.cloneNode(true))
  catalogImageCard.addEventListener('click', () => { loadImageForAnalysis(catalogImage.src, false) })
  targetRow.prepend(catalogImageCard)

  targetCard.innerHTML = ''
  targetCard.append(imageContainer)
}

// ========== Event Definitions ==========

// Load the cocoSSD library after 500ms to enable the browser to render CSS properly

uploadBtn.addEventListener('click', () => {
  imageInput.click()
})
centerPanel.classList.remove('loading-panel')

leftCatalogCover.addEventListener('click', showLeftCatalogModal)
rightCatalogCover.addEventListener('click', showRightCatalogModal)
overlay.addEventListener('click', hideLeftCatalogModal)
overlay.addEventListener('click', hideRightCatalogModal)

if(!loggedIn()) {
  // Open the Login Modal when the user clicks on the Login button
  loginBtn.addEventListener('click', showLoginModal)

  // Hide the Login Modal and open the Sign In Modal when the user clicks on the Sign In link
  signInLink.addEventListener('click', () => {
    hideLoginModal()
    showSignInModal()
  })

  // Hide the Sign In Modal and open the Login Modal when the user clicks on the Log In link
  loginLink.addEventListener('click', () => {
    hideSignInModal()
    showLoginModal()
  })

  // Hide the Login Modal when the user clicks out of it
  overlay.addEventListener('click', hideLoginModal)
  overlay.addEventListener('click', hideSignInModal)

  loginModal.querySelector('form').addEventListener('submit', (e) => {
    e.preventDefault()
    attemptLogIn()
  })

  signInModal.querySelector('form').addEventListener('submit', (e) => {
    e.preventDefault()
    attemptSignIn()
  })
} else {
  avatar.addEventListener('click', toggleUserMenu)
  // logoutBtn.addEventListener('click', toggleUserMenu)
}

// Code which runs when an image is chosen by the user
imageInput.addEventListener('change', async (event) => {
  let file = event.target.files[0]
  let url = await readURL(file)

  // end the script when no image has been read form the input
  if(url == null) return

  // detect objects on the image
  loadImageForAnalysis(url)
})

document.querySelectorAll(".catalog-modal .image-card").forEach((el) => {
  el.addEventListener('click', (e) => {
    loadImageForAnalysis(e.target.src, false)
  })
})

if(window.location.href.includes('registered=true')) {
  let newUrl = window.location.href.replace('?registered=true', '')
  history.pushState({}, null, newUrl)
  displayNotice('Account created', 2000)
} else if(window.location.href.includes('logged_in=true')) {
  let newUrl = window.location.href.replace('?logged_in=true', '')
  history.pushState({}, null, newUrl)
  displayNotice('Logged In', 2000)
}


// export functions
exports.sendRequest = sendRequest;
exports.capitalize = capitalize;
exports.startLoadingAnimation = startLoadingAnimation;
exports.stopLoadingAnimation= stopLoadingAnimation;
exports.showOverlay = showOverlay;
exports.hideOverlay = hideOverlay;
exports.showLoginModal = showLoginModal;
exports.hideLoginModal = hideLoginModal;
exports.showSignInModal= showSignInModal;
exports.hideSignInModal = hideSignInModal;
exports.toggleUserMenu = toggleUserMenu;
exports.displayInfo = displayInfo;
exports.displayNotice = displayNotice;
exports.displayAlert = displayAlert;
exports.setCanvasSize = setCanvasSize;
exports.drawScaledPredictionRectangle = drawScaledPredictionRectangle;
exports.formError = formError;
exports.cleanFormErrors = cleanFormErrors;
exports.showFormErrors = showFormErrors;
exports.loadImageForAnalysis = loadImageForAnalysis;