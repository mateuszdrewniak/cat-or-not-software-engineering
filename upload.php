<?php
session_start();
require_once('./php/image.php');
header('Content-type:application/json;charset=utf-8');

$response = [];

// get file name
$filename = $_FILES['image_input']['name'];
$image_file_type = pathinfo($filename, PATHINFO_EXTENSION);

$valid_extensions = ["jpg", "jpeg", "png"];

// Check file extension
if(!in_array(strtolower($image_file_type), $valid_extensions)) {
    header('HTTP/1.1 422 Unprocessable Entity');
    $response['error'] = 'Incorrect file type!';
    echo json_encode($response);
    return;
} 

if(!isset($_SESSION['user_id'])) {
    header('HTTP/1.1 403 Forbidden');
    $response['error'] = 'Not logged in!';
    echo json_encode($response);
    return;
}

$image_properties = [
    'extension' => $image_file_type,
    'best_prediction' => $_POST['best_prediction'],
    'user_id' => $_SESSION['user_id']
];

$image = new Image($image_properties);

if($image->persist()) {
    if(move_uploaded_file($_FILES['image_input']['tmp_name'], $image->location())) {
        $response['image'] = $image->to_json();
        header('HTTP/1.1 201 Created');
    } else {
        $image->delete();
        $response['errors'] = 'Unable to save file!';
        $response['destination'] = $image->location();
        header('HTTP/1.1 400 Bad Request');
    }
} else {
    $response['errors'] = $image->get_errors();
    header('HTTP/1.1 400 Bad Request');
}


echo json_encode($response);

?>
