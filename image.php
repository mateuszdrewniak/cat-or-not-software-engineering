<?php
session_start();
require_once('./php/image.php');

if(!(isset($_GET['name']) && isset($_SESSION['user_id']))) {
    echo 'No such image';
    return;
}

$name = $_GET['name'];
$user_id = $_SESSION['user_id'];

$images = Image::where('user_id = ? AND name = ?', [$user_id, $name]);

if(count($images) == 0) {
    echo 'No such image';
    return;
}

$image = $images[0];

$finfo = finfo_open(FILEINFO_MIME_TYPE);
$mime_type = finfo_file($finfo, $image->location());
finfo_close($finfo);

switch ($mime_type){
    case "image/jpeg":
        header('Content-Type: image/jpeg');
        $img = imagecreatefromjpeg($image->location());

        // Output the image
        imagejpeg($img);
        break;
    case "image/png":
        header('Content-Type: image/png');
         
        // Get image from file
        $img = imagecreatefrompng($image->location());
         
        // integer representation of the color black (rgb: 0,0,0)
        $background = imagecolorallocate($img, 0, 0, 0);
         
        // removing the black from the placeholder
        imagecolortransparent($img, $background);
         
        // turning off alpha blending (to ensure alpha channel information 
        // is preserved, rather than removed (blending with the rest of the 
        // image in the form of black))
        imagealphablending($img, false);
         
        // turning on alpha channel information saving (to ensure the full range 
        // of transparency is preserved)
        imagesavealpha($img, true);
         
        // Output the image
        imagepng($img);
         
        break;
}
 
// Free up memory
imagedestroy($img);

?>
