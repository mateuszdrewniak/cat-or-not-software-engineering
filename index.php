<?php
  require_once('./php/image.php');
  session_start();

  $cat_images = [];
  $not_cat_images = [];

  if(isset($_SESSION['user_id'])) {
    $cat_images = Image::where("user_id = ? AND best_prediction = ? ORDER BY id DESC", [$_SESSION['user_id'], 'cat']);
    $not_cat_images = Image::where("user_id = ? AND best_prediction != ? ORDER BY id DESC", [$_SESSION['user_id'], 'cat']);
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Cat or Not?</title>
  <meta name="description" content="Cat or Not?">
  <meta name="author" content="Mateusz Drewniak">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=0.9, user-scalable=no">
  <link rel="stylesheet" href="css/styles.css">
  <!-- Define Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
  <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
  <link rel="manifest" href="site.webmanifest">
  <!-- Load Font Awesome -->
  <script src="https://kit.fontawesome.com/40976a7d9a.js" crossorigin="anonymous"></script>
  <!-- Load TensorFlow.js. This is required to use coco-ssd model. -->
  <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs"> </script>
  <!-- Load the coco-ssd model. -->
  <script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/coco-ssd"> </script>
</head>

<body class="bg-light-gray text-black">
  <div id="info" class="info hidden">
    <p></p>
  </div>

  <!-- ########### MODALS ########### -->
  <div id="overlay" class="hidden"></div>

  <div id="left-catalog-modal" class="catalog-modal modal card shadow bg-white hidden">
    <h3 class="modal-header">Not Cats</h3>
    <div class="modal-body">
      <?php if(count($not_cat_images) == 0) { ?>
        <div class="empty-message">
          No images yet!
        </div>
      <?php } else {
        $item = 0;
        while($item < count($not_cat_images)) {
          
          echo <<<EOL
      <div class="catalog-row">
EOL;
          for($i = 0; $i < 3 && $item < count($not_cat_images); $i++) {
            $current_image = $not_cat_images[$item];
            $path = $current_image->location();

            echo <<<EOL
        <div class="image-card">
          <div class="image-container">
            <div class="best-prediction">$current_image->best_prediction</div>
            <img src="$path">
          </div>
        </div>
EOL;

            $item++;
          }
          echo <<<EOL
      </div>
EOL;
        }
      } ?>
    </div>
  </div>

  <div id="right-catalog-modal" class="catalog-modal modal card shadow bg-white hidden">
    <h3 class="modal-header">Cats</h3>
    <div class="modal-body">
      <?php if(count($cat_images) == 0) { ?>
        <div class="empty-message">
          No images yet!
        </div>
      <?php } else {
        $item = 0;
        while($item < count($cat_images)) {
          
          echo <<<EOL
      <div class="catalog-row">
EOL;
          for($i = 0; $i < 3 && $item < count($cat_images); $i++) {
            $current_image = $cat_images[$item];
            $path = $current_image->location();

            echo <<<EOL
        <div class="image-card">
          <div class="image-container">
            <div class="best-prediction">$current_image->best_prediction</div>
            <img src="$path">
          </div>
        </div>
EOL;

            $item++;
          }
          echo <<<EOL
      </div>
EOL;
        }
      } ?>
    </div>
  </div>

  <?php if(!isset($_SESSION['user_id'])) { ?>
    <!-- Sign In Modal -->
    <div id="sign-in-modal" class="modal card shadow bg-white hidden">
      <h3 class="modal-header">Sign In</h3>
      <div class="modal-body">
        <form action="./sign_in.php" method="post">
          <div class="input user_email">
            <label for="sign-in-email">Email</label>
            <input type="email" name="email" id="sign-in-email" required="true">
          </div>
          <div class="input user_password">
            <label for="sign-in-password">Password</label>
            <input type="password" name="password" id="sign-in-password" required="true">
          </div>
          <div class="input">
            <label for="sign-in-password-confirmation">Password Confirmation</label>
            <input type="password" name="password-confirmation" id="sign-in-password-confirmation" required="true">
          </div>
    
          <div class="input">
            <input type="submit" name="commit" value="Sign In" class="btn bg-blue text-white shadow rounded-full">
          </div>
        </form>
        <div id="login-link" class="link">Log in</div>
      </div>
    </div>

    <!-- Log In Modal -->
    <div id="login-modal" class="modal card shadow bg-white hidden">
      <h3 class="modal-header">Log In</h3>
      <div class="modal-body">
        <form action="./login.php" method="post">
          <div class="input">
            <label for="login-email">Email</label>
            <input type="email" name="email" id="login-email" required="true">
          </div>
          <div class="input">
            <label for="login-password">Password</label>
            <input type="password" name="password" id="login-password" required="true">
          </div>
    
          <div class="input">
            <input type="submit" name="commit" value="Log In" class="btn bg-blue text-white shadow rounded-full">
          </div>
        </form>
        <div id="sign-in-link" class="link">Sign in</div>
      </div>
    </div>

  <?php } else { ?>
    <!-- User Menu -->
    <div id="user-menu" class="card shadow bg-white hidden">
      <div class="input">
        <label>User</label>
        <div class="input-mock disabled"><?php echo $_SESSION['user_email']; ?></div>
      </div>
      <a href="./logout.php" id="logout-btn" class="btn shadow center bg-blue text-white rounded-full mb-2">Log Out</a>
      <a href="./destroy_session.php" id="destroy-session-btn" class="btn shadow center bg-red text-white rounded-full">Destroy Session</a>
    </div>

  <?php } ?>

  <!-- ########### MAIN PAGE ########### -->

  <header class="flex">
    <div class="flex-1">
      <?php if(isset($_SESSION['user_id'])) { ?>
        <img id="avatar" class="rounded-full" src="https://www.gravatar.com/avatar/<?php echo md5(strtolower(trim($_SESSION['user_email']))); ?>">
      <?php } else { ?>
        <div id="login-btn" class="btn shadow center bg-white rounded-full">Log In</div>
      <?php } ?>
    </div>
    <div id="logo" class="center-panel flex-1">
      Cat or Not?
    </div>
    <div class="flex-1">

    </div>
  </header>

  <main class="flex">
    <section id="left-panel" class="panel flex-1">
      <div class="image-card-wrapper">
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div class="image-card"></div>
        <?php if(count($not_cat_images) > 0) { ?>
          <div id="left-catalog-cover" class="image-card">
            <div class="image-container">
              <div class="best-prediction"><?php echo $not_cat_images[0]->best_prediction ?></div>
              <img src="<?php echo $not_cat_images[0]->location() ?>">
            </div>
          </div>
        <?php } else { ?>
          <div id="left-catalog-cover" class="image-card"></div>
        <?php } ?>
        <div class="image-card-wrapper-title">Not cats</div>
      </div>
    </section>

    <section id="center-panel" class="center-panel loading-panel panel flex-1 relative">
      <div class="card shadow bg-white mb-2">
        <div class="card-image relative">
          <canvas id="canvas"></canvas>
          <div class="placeholder-image"></div>
          <div class="loading-spinner spinner">
            <i class="fas fa-circle-notch"></i>
          </div>
        </div>

        <div class="predictions-wrapper">
          <h3>Predictions:</h3>
          <div id="predictions">Cat</div>
        </div>

        <div class="loading-predictions-wrapper">
          <div class="loading-text"></div>
          <div class="loading-text"></div>
        </div>
      </div>
      <div id="upload-btn" class="upload-btn btn shadow mx-auto center bg-blue text-white rounded-full">Choose a Photo</div>
    </section>

    <section id="right-panel" class="panel flex-1">
      <div class="image-card-wrapper">
        <div class="image-card"></div>
        <div class="image-card"></div>
        <div class="image-card"></div>
        <?php if(count($cat_images) > 0) { ?>
          <div id="right-catalog-cover" class="image-card">
            <div class="image-container">
              <div class="best-prediction"><?php echo $cat_images[0]->best_prediction ?></div>
              <img src="<?php echo $cat_images[0]->location() ?>">
            </div>
          </div>
        <?php } else { ?>
          <div id="right-catalog-cover" class="image-card"></div>
        <?php } ?>
        <div class="image-card-wrapper-title">Cats</div>
      </div>
    </section>
  </main>

  <form id="image-form" class="hidden" enctype="multipart/form-data">
    <input type="file" name="image_input" id="image-input" accept=".jpg,.jpeg,.png">
  </form>

  <script src="js/main.js"></script>
</body>
</html>