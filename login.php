<?php
session_start();
require_once('./php/user.php');
header('Content-type:application/json;charset=utf-8');

// Converts raw data from the request into a PHP object
$data = json_decode(file_get_contents('php://input'));

$response = [];

if(isset($_SESSION['user_id'])) {
    header('HTTP/1.1 400 Bad Request');
    $response['error'] = 'Already logged in!';
    echo json_encode($response);
    return;
}

try {
    $user = User::find_by_credentials($data);
    if(is_null($user)) { 
        $response['error'] = 'Wrong email or password';
        header('HTTP/1.1 404 Not Found');
    } else {
        $response['logged_in'] = true;
    }
} catch(PDOException $e) {
    header('HTTP/1.1 500 Internal Server Error');
    $response['error'] = 'Ooops.. Something went wrong!';
}

if(isset($user)) {
    $_SESSION['user_id'] = $user->id;
    $_SESSION['user_email'] = $user->email;
}

echo json_encode($response);

?>
