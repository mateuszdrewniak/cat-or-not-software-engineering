<?php
require_once('./php/connection.php');

class ActiveRecord {
    public $id;
    public $created_at;
    public $updated_at;
    protected $errors = [];
    protected $connection;
    
    public static $attribute_names = [];
    public static $belongs_to = [];
    public static $table_name = '';

    public function __construct($data, $eager = true) {

    }

    public static function find($id) {
        if($id == null) return null;

        $connection = new Connection();
        $sql = "SELECT * FROM ". static::$table_name ." WHERE id = ?";
        $query = $connection->pdo->prepare($sql);
        $query->execute([$id]);
        $row = $query->fetch(PDO::FETCH_ASSOC);

        $connection = null;

        if(!($row !== false)) return null;
        
        $class_name = get_called_class();
        return new $class_name($row);
    }

    public static function where($where_string, $values = null) {
        if(!isset($where_string)) return null;

        $connection = new Connection();

        $sql = "SELECT * FROM ". static::$table_name ." WHERE ". $where_string;
        $query = $connection->pdo->prepare($sql);
        $success = isset($values) ? $query->execute($values) : $query->execute();

        $class_name = get_called_class();
        $result = [];
        if($success) {
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $result[] = new $class_name($row, false);
            }
        }

        $connection = null;

        return $result;
    }

    public static function delete_where($where_string, $values = null) {
        if(!isset($where_string)) return null;

        $connection = new Connection();

        $sql = "DELETE FROM ". static::$table_name ." WHERE ". $where_string;
        $query = $connection->pdo->prepare($sql);
        $success = isset($values) ? $query->execute($values) : $query->execute();

        $connection = null;
    }

    public static function guid($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
    
        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    
        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function all_attribute_names() {
        $attributes = [
            'id',
            'created_at',
            'updated_at'
        ];

        foreach(static::$attribute_names as $attr) {
            $attributes[] = $attr;
        }

        return $attributes;
    }

    public static function attribute_name_list() {
        $attributes = static::$attribute_names[0];
        foreach(static::$attribute_names as $attr) {
            if($attr == static::$attribute_names[0]) continue;
            $attributes = $attributes. ", " .$attr;
        }

        return $attributes;
    }

    public static function attribute_quote_list() {
        $attributes = '?';
        foreach(static::$attribute_names as $attr) {
            if($attr == static::$attribute_names[0]) continue;
            $attributes = $attributes. ", ?";
        }

        return $attributes;
    }

    public function delete() {
        if(!isset($this->id)) return;

        try {
            $this->connect();

            $sql = "DELETE FROM ". static::$table_name ." WHERE id = ?";
            $query = $this->connection->pdo->prepare($sql);
            $query->execute([$this->id]);
            $this->id = null;

            $this->disconnect();
        } catch (PDOException $e) {

        }
    }

    public function set_belongs_to() {
        foreach(static::$belongs_to as $class_name => $foreign_key) {
            if($this->$foreign_key == null) continue;

            $attribute_name = strtolower($class_name);
            try {
                $this->$attribute_name = $class_name::find($this->$foreign_key);
            } catch (PDOException $e) {
                $this->$attribute_name = null;
            }
        }
    }

    public function attribute_values() {
        $values = [];
        foreach(static::$attribute_names as $attr) {
            $values[] = $this->$attr;
        }

        return $values;
    }

    public function valid() {
        if(count($this->errors) != 0) return false;

        return true;
    }

    public function to_json() {
        $json = [];

        foreach(static::all_attribute_names() as $attr) {
            $json[$attr] = $this->$attr;
        }

        return $json;
    }

    public function get_errors() {
        return $this->errors;
    }

    public function persist() {
        if(!$this->valid()) return false;

        try {
            $this->connect();
            $sql = "INSERT INTO ". static::$table_name ." (". static::attribute_name_list() .") VALUES (". static::attribute_quote_list() .")";
            $query = $this->connection->pdo->prepare($sql);
            $query->execute($this->attribute_values());
            $this->id = $this->connection->pdo->lastInsertId();

            $sql = "SELECT * FROM ". static::$table_name ." WHERE id = ?";
            $query = $this->connection->pdo->prepare($sql);
            $query->execute([$this->id]);
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $this->created_at = $row['created_at'];
            $this->updated_at = $row['updated_at'];
            $this->disconnect();

            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    protected function connect() {
        $this->connection = new Connection();
    }

    protected function disconnect() {
        $this->connection = null;
    }

    protected function add_error($param, $error) {
        $this->errors[$param][] = $error;
        $this->errors[$param] = array_values($this->errors[$param]);
    }
}

?>
