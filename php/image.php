<?php
require_once('./php/active_record.php');
require_once('./php/user.php');

class Image extends ActiveRecord {
    public $best_prediction;
    public $name;
    public $user_id;
    public $user;

    public static $attribute_names = ['best_prediction', 'name', 'user_id'];
    public static $belongs_to = ['User' => 'user_id'];
    public static $table_name = 'images';

    public function __construct($data, $eager = true) {
        if(array_key_exists('best_prediction', $data)) $this->best_prediction = $data['best_prediction'];
        if(array_key_exists('name', $data)) $this->name = $data['name'];
        if(array_key_exists('extension', $data)) $this->set_name($data['extension']);
        if(array_key_exists('user_id', $data)) $this->user_id = $data['user_id'];
        if(array_key_exists('id', $data)) $this->id = $data['id'];
        if($eager) $this->set_belongs_to();
    }

    public function set_name($extension) {
        $this->connect();
        while(true) {
            $this->name = static::guid(). '.' .$extension;
            $sql = "SELECT * FROM images WHERE name = ?";
            $query = $this->connection->pdo->prepare($sql);
            $query->execute([$this->name]);

            $row = $query->fetch(PDO::FETCH_ASSOC);
            $this->disconnect();
            if($row) continue;
            break;
        }
        $this->disconnect();
    }

    public function cat() {
        return $this->best_prediction == 'cat';
    }

    public function location() {
        return "uploads/" . $this->name;
    }

    public function address() {
        return 'image.php?name=' . $this->name;
    }

    public function to_json() {
        $json = parent::to_json();
        $json['path'] = $this->location();

        return $json;
    }

    public function valid() {
        if($this->user == null) $this->add_error('user', 'User is incorrect or does not exist!');
        if($this->name == null) $this->add_error('name', 'File name is incorrect!');
        if($this->best_prediction == null) $this->add_error('best_prediction', 'Best prediction must not be empty!');

        return parent::valid();
    }
}

?>
