<?php
class Connection {
    public $name;
    public $user;
    public $password;
    public $host;
    public $pdo;

    public function __construct() {
        $config = parse_ini_file('config.ini');
        $this->name = $config['name'];
        $this->user = $config['user'];
        $this->password = $config['password'];
        $this->host = $config['host'];
        $this->pdo = new PDO("mysql:host=". $this->host .";dbname=". $this->name, $this->user, $this->password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}

?>
