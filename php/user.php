<?php
require_once('./php/active_record.php');

class User extends ActiveRecord {
    public $email;
    protected $encrypted_password;

    public static $attribute_names = ['email', 'encrypted_password'];
    public static $table_name = 'users';

    public function __construct($data, $eager = true) {
        if(!is_null($data) && array_key_exists('email', $data)) {
            if(array_key_exists('password_confirmation', $data)) $password_confirmation = $data['password_confirmation'];
            if(array_key_exists('id', $data)) $this->id = $data['id'];
            if(array_key_exists('password', $data)) $this->set_password($data['password'], $password_confirmation);
            if(array_key_exists('encrypted_password', $data)) $this->encrypted_password = $data['encrypted_password'];
            if(array_key_exists('created_at', $data)) $this->created_at = $data['created_at'];
            if(array_key_exists('updated_at', $data)) $this->updated_at = $data['updated_at'];
            $this->set_email($data['email']);
        }
    }

    public function set_password($password, $password_confirmation = null) {
        if(!$this->check_password($password, $password_confirmation)) return;

        $this->encrypted_password = password_hash($password, PASSWORD_BCRYPT);
    }

    public function set_email($email) {
        if(!$this->check_email($email)) return;

        $this->email = $email;
    }

    public static function find_by_credentials($data) {
        if(!(property_exists($data, 'email') && property_exists($data, 'password'))) return null;

        $connection = new Connection();
        $sql = "SELECT id, email, encrypted_password, created_at, updated_at FROM users WHERE email = ?";
        $query = $connection->pdo->prepare($sql);
        $query->execute([$data->email]);
        $row = $query->fetch(PDO::FETCH_ASSOC);

        $connection = null;

        if(!($row !== false)) return null;

        if(!password_verify($data->password, $row['encrypted_password'])) return null;

        $user = new User($row);
        return $user;
    }

    public function valid() {
        if($this->id == null) {
            $this->connect();
            $sql = "SELECT * FROM users WHERE email = ?";
            $query = $this->connection->pdo->prepare($sql);
            $query->execute([$this->email]);

            $row = $query->fetch(PDO::FETCH_ASSOC);
            $this->disconnect();
            if($row) {
                $this->add_error('email', 'Email is already taken!');
                return false;
            }
        }

        if(count($this->errors) != 0 || is_null($this->email) || is_null($this->encrypted_password)) return false;

        return true;
    }

    public function to_json() {
        $json = [
            'id' => $this->id,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];

        return $json;
    }

    protected function check_email($email) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->add_error('email', 'Email is invalid!');
            return false;
        }

        return true;
    }

    protected function check_password($password, $password_confirmation) {
        $errors_init = $this->errors;
        if(!is_null($password_confirmation)) {
            if($password != $password_confirmation){
                $this->add_error('password', 'Passwords do not match!');
            }
        }

        if(strlen($password) < 6) {
            $this->add_error('password', 'Password is too short (min 6 characters)');
        }

        if (!preg_match("#[0-9]+#", $password)) {
            $this->add_error('password', 'Password must include at least one number!');
        }
    
        if (!preg_match("#[a-zA-Z]+#", $password)) {
            $this->add_error('password', 'Password must include at least one letter!');
        }  

        return ($this->errors == $errors_init);
    }
}

?>
