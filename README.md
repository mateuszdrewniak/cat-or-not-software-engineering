# Cat or Not Web App

Cat or Not is a web app which recognises cats (and other objects) on images sent by the user. It's dynamic and is driven by TensorFlow.js. The majority of the frontend to backend communication is designed asynchronously - there is no need to reload the whole site.

## Stack - technologies

The app makes use of the simplest stack which enables it to meet its goals:

- Frontend: JavaScript with TensorFlow.js and regular CSS
- Backend: regular PHP
- Database: MySQL
- Language of communication between the frontend and the backend: JSON
- Web Server: NGINX

The app is available at this address: https://cat-or-not.mateuszdrewniak.it/.
Object recognition is achieved with the use of TensorFlow.js and its coco-ssd model.

It's possible to create an account and log in in order to save checked images. In that case images are saved on the server after they're evaluated by the frontend. You can also delete all saved image on demand.

## REST API

The app uses a simple REST API with a couple of endpoints to let users log in, sign in and save images.

### Endpoints

#### POST /sign_in.php

Sign in - create a new user account. Communication in JSON.

##### Request example

```json
{
   "email":"libfejfe@gmail.com",
   "password":"sldhflskdhfl234",
   "password_confirmation":"sldhflskdhfl234"
}
```

##### Response examples

- Success, HTTP 201

```json
{
   "user":{
      "id":"21",
      "email":"libfejfe@gmail.com",
      "created_at":"2021-01-06 22:31:27",
      "updated_at":"2021-01-06 22:31:27"
   }
}
```

- Bad request format, HTTP 400

```json
{
   "error":"Incorrect format!"
}
```

- There is an account with this email, HTTP 400

```json
{
   "errors":{
      "email":[
         "Email is already taken!"
      ]
   }
}
```

- A couple of parameter errors, HTTP 400

```json
{
   "errors":[
      {
         "email":[
            "Email is already taken!"
         ]
      },
      {
         "password":[
            "Passwords do not match!",
            "Password is too short (min 6 characters)"
         ]
      }
   ]
}
```

#### POST /login.php

Log in to an existing account. Communication in JSON.

##### Request example

```json
{
   "email":"libfejfe@gmail.com",
   "password":"sldhflskdhfl234"
}
```

##### Response examples

- Logged in, HTTP 200

```json
{
   "logged_in":true
}
```

- An attempt of logging in when the user is already logged in, HTTP 400

```json
{
   "error":"Already logged in!"
}
```

- No such user, HTTP 400

```json
{
   "error":"Wrong email or password!"
}
```

#### POST /upload.php

Save an image on the server and make it available. Takes in 'multipart/form-data' (.png or .jpg files) and responds with JSON.

##### Parameters example

```js
image_input: "image.png"
```

##### Response examples

- Image has been saved, HTTP 200

```json
{
   "image":{
      "id":"26",
      "created_at":"2021-01-24 00:00:52",
      "updated_at":"2021-01-24 00:00:52",
      "best_prediction":"Person",
      "name":"6fdb98a1-599a-4ca0-ad65-cc9647ef52fd.png",
      "user_id":"21",
      "path":"uploads/6fdb98a1-599a-4ca0-ad65-cc9647ef52fd.png"
   }
}
```

- Not logged in, HTTP 403

```json
{
   "error": "Not logged in!"
}
```

- Unable to save the image, HTTP 400

```json
{
   "error":"Unable to save file!",
   "destination":"uploads/6fdb98a1-599a-4ca0-ad65-cc9647ef52fd.png"
}
```

#### GET /image.php

Renders one of the saved images.

##### Parameters example

```js
name: "image.png"
```

## Backend architecture

### ActiveRecord

#### The idea

This app makes use of the `ActiveRecord` pattern (based on `Ruby on Rails` implementation of it). This means that there are no specific classes made for interacting with the database. Instead, there are `models` or classes/objects which are the exact representations of a single row from certain database tables. Every one of these objects is capable of saving itself to the database and of doing inquires regarding the table that they represent. All `models` lie in the `/php` directory.

`ActiveRecord` is great because of the fact that you receive objects with methods from the database instead of raw data.

All of this is possible because of the abstract class of `ActiveRecord` which grants the classes that inherit from it a quite simple but powerful API for communicating with the database. Connections are made with the use of the `Connect` class.

All descendants of the `ActiveRecord` automatically posses three table fields: `id, created_at, updated_at`. This means that every table MUST have all of these three fields.

Every class descending from `ActiveRecord` must also implement the static fields of:
```php
public static $attribute_names = ['some_field', 'another_field']; // additional fields of the table represented by this class other than [id, created_at, updated_at]
public static $table_name = 'table_name'; // name of the table represented by this class
```

There is also an easy way to indicate relations:
```php
public $class; // name of the variable which will hold the ActiveRecord object of the related table; 
// it must be a lowercase version of the related class name! So if the related class is 'Image' then this variable must be named `$image`!
public static $belongs_to = ['Class' => 'foreign_key']; // ManyToOne relation where 'Class' represents the name of the class which represents the table that is in relation
// `foreign_key` represents the name of the foreign key of this relation in the current class
```

`ActiveRecord` will use this information to automatically load the row of the related db table as an object.

All classes have constructors which take two parameters:

- `$data` - should be an associative array with keys equal to table colums from db and values equal to values of these fields
- `$eager` - a boolean value. When `false` then related objects will not be loaded, when `true` they will automatically be loaded (set to `false` in inquires to the database)
  
#### Examples

```php
// load ActiveRecord classes
require_once('./php/image.php');
require_once('./php/user.php');

// Creating a new image
$data = [
   'best_prediction' => 'cat',
   'name' => '2837492837401924.png',
   'user_id' => 2
];

$image = new Image($data); // the second parameter is optional (defaults to `true`)
$user = $image->user; // will show the loaded User instance with id=2 if said user exists in db
$image->id // will return null since the image is not saved to the database

$image->persist(); // saves the image to the database, returns `true` if saved, `false` if not saved
// before attempting to save the image it will be validated with the `valid()` method (which returns `true` or `false`)
// by default this method checks if there are any errors in the `$errors` array
$image->id; // id of the image is now present since it has been saved

$images = Image::where('id = ?', [$image->id]); // this will return an array of Image objects retrieved from the database which fit the WHERE section of the inquiry (in this case only one image)
// this produces and executes 'SELECT * FROM images WHERE id = ' . $image->id
// all of the retrieved rows are then wrapped with the Image object and returned in an array

$image->delete(); // deletes this image from the database
$images = Image::where('id = ?', [$image->id]); // this will now return an empty array since the image has been deleted
$image = Image::find($image->id); // this will attempt to return an image with the passed id
// this produces and executes 'SELECT * FROM images WHERE id = ' . $image->id
// but returns only the first found image and raises an error if it wasn't found




```
