<?php
session_start();
require_once('./php/user.php');
header('Content-type:application/json;charset=utf-8');

function safe_property($object, $property) {
    return property_exists($object, $property) ? $object->$property : null;
}

// Converts raw data from the request into a PHP object
$data = json_decode(file_get_contents('php://input'));
// For testing ;)
// $data = new stdClass;
// $data->password = 'haslo1';
// $data->password_confirmation = 'haslo1';
// $data->email = 'matik@super.pl';

$response = [];

if(is_null($data)) {
    header('HTTP/1.1 400 Bad Request');
    $response['error'] = 'Incorrect format!';
    echo json_encode($response);
    return;
}

if(isset($_SESSION['user_id'])) {
    header('HTTP/1.1 400 Bad Request');
    $response['error'] = 'Already logged in!';
    echo json_encode($response);
    return;
}


$params = ['email' => safe_property($data, 'email'), 'password' => safe_property($data, 'password'), 'password_confirmation' => safe_property($data, 'password_confirmation')];
$user = new User($params);

if($user->persist()) {
    $_SESSION['user_id'] = $user->id;
    $_SESSION['user_email'] = $user->email;
    $response['user'] = $user->to_json();
    header('HTTP/1.1 201 Created');
} else {
    $response['errors'] = $user->get_errors();
    header('HTTP/1.1 400 Bad Request');
}

echo json_encode($response);

?>
